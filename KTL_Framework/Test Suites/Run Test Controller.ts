<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Run Test Controller</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>64fc548a-ef94-4e72-bb99-a3d839d7034f</testSuiteGuid>
   <testCaseLink>
      <guid>1845bf2f-5bc1-49d2-82fb-e6b4da9ba7b7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC001CalculatedFromCarPrice</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>14a07191-77f7-46a7-b325-7352373923f8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC002CalculatedFromMonthlyInstallments</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>658e7475-8266-4f69-b809-10830ffd099d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC003CalculateConditionalCredits</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8cb8991f-19fe-4a76-a0fa-17f348b3d699</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC004 Motorcycle loan calculations</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>15131163-93da-45cc-b9ff-18334d355578</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC005 BorrowingPowerCalculator</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>84aefa4c-823f-4ea9-a09c-e4d0e51f95fa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC06</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
