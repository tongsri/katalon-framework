import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

CustomKeywords.'com.extosoft.keyword.WebKeyword.FW_OpenBrowser'('https://www.krungsrimarket.com/CalculateLoan', 'Open Web krungsri Calculate Loan')

CustomKeywords.'com.extosoft.keyword.WebKeyword.FW_Click'(findTestObject('Krungsrimarket/Calculated from monthly/Page_/Tab Calculated from monthly tab'), 
    'Click Calculated from monthly TAB')

CustomKeywords.'com.extosoft.keyword.WebKeyword.FW_SetText'(findTestObject('Krungsrimarket/Calculated from monthly/Page_/input_ ( VAT)_txtTab2PeriodPayValue'), 
    'Input Period Pay Value', '10000')

CustomKeywords.'com.extosoft.keyword.WebKeyword.FW_SetText'(findTestObject('Krungsrimarket/Calculated from monthly/Page_/input__txtTab2DownPaymentValue'), 
    'Input Down Payment Value', '500000')

CustomKeywords.'com.extosoft.keyword.WebKeyword.FW_SetText'(findTestObject('Krungsrimarket/Calculated from monthly/Page_/input__txtTab2RateCutOff'), 
    'Input Rate Cut OFF', '5')

CustomKeywords.'com.extosoft.keyword.WebKeyword.FW_Click'(findTestObject('Krungsrimarket/Calculated from monthly/Page_/input__btnTab2CalculateLoan'), 
    'Click Button Calculate Loan')

CustomKeywords.'com.extosoft.keyword.WebKeyword.FW_CheckMessage'(findTestObject('Krungsrimarket/Calculated from monthly/Page_/Highest recommended car price per 72 Month'), 
    'Check Highest recommended car price per 12 Month', '888888')
//574,099
CustomKeywords.'com.extosoft.keyword.WebKeyword.FW_CloseBrowser'('https://www.krungsrimarket.com/CalculateLoan', 'Web krungsri Calculate Loan')

