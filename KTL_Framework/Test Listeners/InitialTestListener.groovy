import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject

import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import internal.GlobalVariable as GlobalVariable

import com.kms.katalon.core.annotation.BeforeTestCase
import com.kms.katalon.core.annotation.BeforeTestSuite
import com.kms.katalon.core.annotation.AfterTestCase
import com.kms.katalon.core.annotation.AfterTestSuite
import com.kms.katalon.core.context.TestCaseContext
import com.kms.katalon.core.context.TestSuiteContext

import com.kms.katalon.core.util.KeywordUtil
import com.extosoft.keyword.UtilityKeyword
import com.extosoft.keyword.ExcelKeyword

import org.apache.poi.hssf.usermodel.HSSFWorkbook
import org.apache.poi.ss.usermodel.*
import org.apache.poi.ss.util.CellReference
import org.apache.poi.ss.util.WorkbookUtil

import org.apache.poi.xssf.usermodel.XSSFCell as XSSFCell
import org.apache.poi.xssf.usermodel.XSSFRow as XSSFRow
import org.apache.poi.xssf.usermodel.XSSFSheet as XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook as XSSFWorkbook
import java.util.Date
import java.lang.Integer

class InitialTestListener {
	
	
	def UtilityKeyword util = new UtilityKeyword()
	def ExcelKeyword excelKw = new ExcelKeyword()
	def XSSFWorkbook xssfWb = null
	def Sheet sh = null
	def XSSFRow xssfRw = null;
	
	/**
	 * Executes before every test case starts.
	 * @param testCaseContext related information of the executed test case.
	 */
	@BeforeTestCase
	def sampleBeforeTestCase(TestCaseContext testCaseContext) {
//		println testCaseContext.getTestCaseId()
//		println testCaseContext.getTestCaseVariables()
		
		KeywordUtil.logInfo("********* SampleBeforeTestCase " + testCaseContext.getTestCaseId() + " : " + testCaseContext.getTestCaseStatus() + " *********")
		
		String testCaseId = testCaseContext.getTestCaseId()
		KeywordUtil.logInfo("TestCaseId : " + testCaseId)
		GlobalVariable.currentTestCaseName = testCaseId.substring(testCaseId.indexOf('/') + 1)
//		WebUI.comment("#sampleBeforeTestCase GlobalVariable.currentTestCaseName=${GlobalVariable.currentTestCaseName}")
		GlobalVariable.testCaseExecuteStatus = "NOT EXECUTED"
		GlobalVariable.runningStep = 1
//		excelKw = new ExcelKeyword()
		xssfWb = excelKw.getWorkbook(GlobalVariable.outputReportFolderName + GlobalVariable.outputReportFileName)
		KeywordUtil.logInfo("Get ExcelController : " + GlobalVariable.outputReportFolderName + GlobalVariable.outputReportFileName)
		sh = excelKw.getExcelSheet(xssfWb, "TestController")
		KeywordUtil.logInfo("Get SheetName : " + sh.getSheetName())
		KeywordUtil.logInfo("Get currentRowTestCase : " + GlobalVariable.currentRowTestCase)
		
		xssfRw = sh.getRow(GlobalVariable.currentRowTestCase)
		
		XSSFCell testCase = xssfRw.getCell(0)
		XSSFCell testName = xssfRw.getCell(1)
		XSSFCell tag = xssfRw.getCell(2)
		
		GlobalVariable.testCase =  testCase.getStringCellValue()
		GlobalVariable.testCaseName =  testName.getStringCellValue()
		GlobalVariable.tag = tag.getStringCellValue()
		
		KeywordUtil.logInfo("Get TestCase : " + GlobalVariable.testCase)
		KeywordUtil.logInfo("Get TestCaseName : " + GlobalVariable.testCaseName)
		KeywordUtil.logInfo("Get Tag : " + GlobalVariable.tag)

		if(GlobalVariable.tag == "No Run"){
			KeywordUtil.logInfo("SkipThisTestCase : " + GlobalVariable.testCase)
			xssfWb = excelKw.getWorkbook(GlobalVariable.outputReportFolderName + GlobalVariable.outputReportFileName)
			KeywordUtil.logInfo("ReportResult Open : " + GlobalVariable.outputReportFolderName + GlobalVariable.outputReportFileName)
			sh = excelKw.getExcelSheet(xssfWb, "TestController")
			KeywordUtil.logInfo("Set TestCase ExecuteStatus : " + GlobalVariable.testCaseExecuteStatus)
			excelKw.setValueToCellByIndex(sh, GlobalVariable.currentRowTestCase, 3, GlobalVariable.testCaseExecuteStatus)
			excelKw.saveWorkbook(GlobalVariable.outputReportFolderName + GlobalVariable.outputReportFileName, xssfWb)
			KeywordUtil.logInfo("ReportResult Save : " + GlobalVariable.outputReportFolderName + GlobalVariable.outputReportFileName)
			//GlobalVariable.currentRowTestCase = GlobalVariable.currentRowTestCase + 1
			KeywordUtil.logInfo("********************************************")
			testCaseContext.skipThisTestCase()
		}
		
		KeywordUtil.logInfo("********************************************")
	}

	/**
	 * Executes after every test case ends.
	 * @param testCaseContext related information of the executed test case.
	 */
	@AfterTestCase
	def sampleAfterTestCase(TestCaseContext testCaseContext) {
//		println testCaseContext.getTestCaseId()
//		println testCaseContext.getTestCaseStatus()
		
		KeywordUtil.logInfo("********* SampleAfterTestCase " + testCaseContext.getTestCaseId() + " : " + testCaseContext.getTestCaseStatus() + " *********")
		if(GlobalVariable.tag == "No Run"){
			KeywordUtil.logInfo("SkipThisTestCase : " + GlobalVariable.testCase)
			GlobalVariable.currentRowTestCase = GlobalVariable.currentRowTestCase + 1
			KeywordUtil.logInfo("********************************************")
		}else{
			xssfWb = excelKw.getWorkbook(GlobalVariable.outputReportFolderName + GlobalVariable.outputReportFileName)
			KeywordUtil.logInfo("ReportResult Open : " + GlobalVariable.outputReportFolderName + GlobalVariable.outputReportFileName)
			
			String testCaseStatus = testCaseContext.getTestCaseStatus()
			KeywordUtil.logInfo("Get TestCaseStatus : " + testCaseStatus)
			GlobalVariable.testCaseExecuteStatus = testCaseStatus
			
			if (testCaseStatus == "PASSED"){
				GlobalVariable.totalTestCasesPassed = GlobalVariable.totalTestCasesPassed + 1
			}else if(testCaseStatus == "FAILED"){
				GlobalVariable.totalTestCasesFailed = GlobalVariable.totalTestCasesFailed + 1
				GlobalVariable.executeStatus = "FAIL"
				xssfWb = excelKw.getWorkbook(GlobalVariable.outputReportFolderName + GlobalVariable.outputReportFileName)
				KeywordUtil.logInfo("ReportResult Open : " + GlobalVariable.outputReportFolderName + GlobalVariable.outputReportFileName)
				sh = excelKw.getExcelSheet(xssfWb, "TestStep")
				excelKw.setValueToCellByIndex(sh, GlobalVariable.currentRowStep - 1, 2, testCaseStatus)
				excelKw.saveWorkbook(GlobalVariable.outputReportFolderName + GlobalVariable.outputReportFileName, xssfWb)
				KeywordUtil.logInfo("ReportResult Save : " + GlobalVariable.outputReportFolderName + GlobalVariable.outputReportFileName)
				KeywordUtil.logInfo("********************************************")
			}
	
			sh = excelKw.getExcelSheet(xssfWb, "TestController")
			KeywordUtil.logInfo("Get SheetName : " + sh.getSheetName())
			KeywordUtil.logInfo("Get CurrentRowTestCase : " + GlobalVariable.currentRowTestCase)
			
			GlobalVariable.totalTestCasesExecuted  = GlobalVariable.totalTestCasesExecuted + 1
			KeywordUtil.logInfo("TotalTestCasesExecuted : " + GlobalVariable.totalTestCasesExecuted)
			KeywordUtil.logInfo("TotalTestCasesPassed : " + GlobalVariable.totalTestCasesPassed)
			KeywordUtil.logInfo("TotalTestCasesFailed : " + GlobalVariable.totalTestCasesFailed)
			
			KeywordUtil.logInfo("Set TestCase ExecuteStatus : " + testCaseStatus)
			excelKw.setValueToCellByIndex(sh, GlobalVariable.currentRowTestCase, 3, GlobalVariable.testCaseExecuteStatus)
			excelKw.saveWorkbook(GlobalVariable.outputReportFolderName + GlobalVariable.outputReportFileName, xssfWb)
			KeywordUtil.logInfo("ReportResult Save : " + GlobalVariable.outputReportFolderName + GlobalVariable.outputReportFileName)
			GlobalVariable.currentRowTestCase = GlobalVariable.currentRowTestCase + 1
			KeywordUtil.logInfo("********************************************")
		}
		
		GlobalVariable.runningStep = 0
	}

	/**
	 * Executes before every test suite starts.
	 * @param testSuiteContext: related information of the executed test suite.
	 */
	@BeforeTestSuite
	def sampleBeforeTestSuite(TestSuiteContext testSuiteContext) {
//		println testSuiteContext.getTestSuiteId()
		
		KeywordUtil.logInfo("********* SampleBeforeTestSuite " + testSuiteContext.getTestSuiteId() + " *********")	
//		util = new UtilityKeyword()
		String generateDateTime = util.FW_GenerateDateTime()
		GlobalVariable.dateExecuted = util.FW_DateExecuted()
		GlobalVariable.testStartTime = util.FW_TestStartTime()
		
		KeywordUtil.logInfo("********* Create Initial FrameWork *********")
		
		try {
			
			File myFile = new File(GlobalVariable.reportFolderName + generateDateTime);
			if (myFile.mkdir()) {

//				excelKw = new ExcelKeyword()
				xssfWb = excelKw.getWorkbook(GlobalVariable.excelController)
				sh = excelKw.getExcelSheet(xssfWb, "SummaryReport")
				excelKw.getCellValueByIndex(sh, 9, 1)
				String currentDateVersion = util.FW_GetCurrentDateVersion()
				GlobalVariable.dateVersion = excelKw.getCellValueByIndex(sh, 8, 1)
				String exVersion = excelKw.getCellValueByIndex(sh, 9, 1)
				GlobalVariable.executeVersion = Integer.parseInt(exVersion)				
				GlobalVariable.executeStatus = "PASS"
				KeywordUtil.logInfo("CurrentDateVersion : " + currentDateVersion)
				KeywordUtil.logInfo("DateVersion : " + GlobalVariable.dateVersion)
				KeywordUtil.logInfo("ExecuteVersion : " + GlobalVariable.executeVersion)
				
				if (GlobalVariable.dateVersion.equals(currentDateVersion)) {
					GlobalVariable.executeVersion = GlobalVariable.executeVersion + 1
					excelKw.setValueToCellByIndex(sh, 9, 1, GlobalVariable.executeVersion)
					KeywordUtil.logInfo("Add ExecuteVersion : " + GlobalVariable.executeVersion)
					excelKw.saveWorkbook(GlobalVariable.excelController, xssfWb)
				}else{
					excelKw.setValueToCellByIndex(sh, 8, 1, currentDateVersion)
					excelKw.setValueToCellByIndex(sh, 9, 1, "1")
					KeywordUtil.logInfo("Set Reset ExecuteVersion : 1")
					KeywordUtil.logInfo("Set New DateVersion : " + currentDateVersion)
					excelKw.saveWorkbook(GlobalVariable.excelController, xssfWb)
				}
				
				GlobalVariable.outputReportFolderName = GlobalVariable.reportFolderName + generateDateTime + "\\"
				GlobalVariable.outputReportFileName = GlobalVariable.outputReportFileName + "_" + generateDateTime + ".xlsx"
				KeywordUtil.logInfo("ReportFolderName Created : " + GlobalVariable.outputReportFolderName)
				KeywordUtil.logInfo("ReportFileName Created : " + GlobalVariable.testStartTime)
				KeywordUtil.logInfo("File Created : " + myFile.getName())
				KeywordUtil.logInfo("ReportResult Created : " + GlobalVariable.outputReportFolderName + GlobalVariable.outputReportFileName)
				
				xssfWb = excelKw.getWorkbook(GlobalVariable.excelController)
				sh = excelKw.getExcelSheet(xssfWb, "SummaryReport")
				excelKw.setValueToCellByIndex(sh, 1, 1, GlobalVariable.dateExecuted)
				excelKw.setValueToCellByIndex(sh, 2, 1, GlobalVariable.testStartTime)
				KeywordUtil.logInfo("Set DateExecuted : " + GlobalVariable.dateExecuted)
				KeywordUtil.logInfo("Set TestStartTime : " + GlobalVariable.testStartTime)
				excelKw.saveWorkbook(GlobalVariable.outputReportFolderName + GlobalVariable.outputReportFileName, xssfWb)
				KeywordUtil.logInfo("ReportResult Save : " + GlobalVariable.outputReportFolderName + GlobalVariable.outputReportFileName)
			} else {
				KeywordUtil.logInfo("File Already Exists.")
			}

		} catch (IOException e) {
			KeywordUtil.logInfo("SampleBeforeTestSuite IOException : " + e.getMessage())
			e.printStackTrace();
		}
		
		KeywordUtil.logInfo("********************************************")
		
	}

	/**
	 * Executes after every test suite ends.
	 * @param testSuiteContext: related information of the executed test suite.
	 */
	@AfterTestSuite
	def sampleAfterTestSuite(TestSuiteContext testSuiteContext) {
//		println testSuiteContext.getTestSuiteId()
		
		KeywordUtil.logInfo("********* SampleAfterTestSuite " + testSuiteContext.getTestSuiteId() + " *********")
		GlobalVariable.testEndTime = util.FW_TestEndTime()
		long durationTime = util.FW_GetDurationTime()
		int duration = (durationTime / 1000)
		KeywordUtil.logInfo("DurationTime : " + duration)
		
		
		double dHour = (duration / 3600)
		String strHour = "" + (Math.floor(dHour))
		KeywordUtil.logInfo("strHour : " + strHour)
		String[] strHourArry = strHour.split("\\.")
		
		if(strHourArry[0].length() == 1){
			strHour = "0" + strHourArry[0]
		}else{
			strHour = "" + strHourArry[0]
		}
		KeywordUtil.logInfo("Get Hour : " + strHour)
		
		double dMinute = (duration / 60)
		String strMinute = "" + (Math.floor(dMinute))
		KeywordUtil.logInfo("strMinute : " + strMinute)
		String[] strMinuteArry = strMinute.split("\\.")
		if(strMinuteArry[0].length() == 1){
			strMinute = "0" + strMinuteArry[0]
		}else{
			strMinute = "" + strMinuteArry[0]
		}
		KeywordUtil.logInfo("Get Minute : " + strMinute)
		
		int dSecond = (duration % 60)
		KeywordUtil.logInfo("dSecond : " + dSecond)
		String strSecond = ""
		if(dSecond < 10){
			strSecond = "0" + dSecond
		}else{
			strSecond = "" + dSecond
		}
		KeywordUtil.logInfo("Get Second : " + strSecond)
		
		String strDurationTime = strHour + ":" + strMinute + ":" + strSecond
		
		
		xssfWb = excelKw.getWorkbook(GlobalVariable.outputReportFolderName + GlobalVariable.outputReportFileName)
		KeywordUtil.logInfo("ReportResult Open : " + GlobalVariable.outputReportFolderName + GlobalVariable.outputReportFileName)
		sh = excelKw.getExcelSheet(xssfWb, "SummaryReport")
		excelKw.setValueToCellByIndex(sh, 3, 1, GlobalVariable.testEndTime)
		excelKw.setValueToCellByIndex(sh, 4, 1, ("" + strDurationTime))
		excelKw.setValueToCellByIndex(sh, 5, 1, GlobalVariable.totalTestCasesExecuted)
		excelKw.setValueToCellByIndex(sh, 6, 1, GlobalVariable.totalTestCasesPassed)
		excelKw.setValueToCellByIndex(sh, 7, 1, GlobalVariable.totalTestCasesFailed)
		
		KeywordUtil.logInfo("Set Test EndTime : " + GlobalVariable.testEndTime)
		KeywordUtil.logInfo("Set DurationTime : " + duration)
		KeywordUtil.logInfo("Set Total TestCases Executed : " + GlobalVariable.totalTestCasesExecuted)
		KeywordUtil.logInfo("Set Total TestCases Passed : " + GlobalVariable.totalTestCasesPassed)
		KeywordUtil.logInfo("Set Total TestCases Failed : " + GlobalVariable.totalTestCasesFailed)
		excelKw.saveWorkbook(GlobalVariable.outputReportFolderName + GlobalVariable.outputReportFileName, xssfWb)
		
		File oldfile = new File(GlobalVariable.outputReportFolderName + GlobalVariable.outputReportFileName);
		String newReportFile = GlobalVariable.outputReportFileName
		newReportFile = newReportFile.replaceAll(".xlsx", "")
		String reportResult = GlobalVariable.outputReportFolderName + newReportFile + "_" + GlobalVariable.executeVersion + "_" +  GlobalVariable.executeStatus + ".xlsx"
		File newfile = new File(reportResult);

		if(oldfile.renameTo(newfile)){
			KeywordUtil.logInfo("Rename Succesful");
			oldfile.delete()
		}else{
			KeywordUtil.logInfo("Rename Failed");
		}
	
		String oldPath = GlobalVariable.outputReportFolderName
		oldPath = oldPath.substring(0, (oldPath.length() - 1))
		KeywordUtil.logInfo("oldPath --> " + oldPath);
		File oldFolder = new File(oldPath);
		String newReportFolder = GlobalVariable.outputReportFileName
		newReportFile = newReportFile.replaceAll(".xlsx", "")
		String reportFolderResult = GlobalVariable.reportFolderName + newReportFile + "_" + GlobalVariable.executeVersion + "_" +  GlobalVariable.executeStatus
		KeywordUtil.logInfo("reportFolderResult --> " + reportFolderResult);
		File newFolder = new File(reportFolderResult);

		if(oldFolder.renameTo(newFolder)){
			KeywordUtil.logInfo("Directory Rename Succesful");
			oldFolder.delete()
		}else{
			KeywordUtil.logInfo("Directory Rename Failed");
		}
		
		KeywordUtil.logInfo("ReportResult Save : " + GlobalVariable.outputReportFolderName + GlobalVariable.outputReportFileName)
		KeywordUtil.logInfo("********************************************")
	
		
	}
}