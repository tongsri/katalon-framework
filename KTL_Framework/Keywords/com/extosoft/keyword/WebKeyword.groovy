package com.extosoft.keyword
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.exception.StepFailedException
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable

import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException


class WebKeyword {

	def UtilityKeyword util = new UtilityKeyword()

	@Keyword
	def void FW_OpenBrowser(String applicationURL) {
		KeywordUtil.logInfo("OpenBrowser : " + applicationURL)
		WebUI.openBrowser(applicationURL)
		FW_MaximizeWindow()
	}

	@Keyword
	def void FW_CloseBrowser(String applicationURL) {
		KeywordUtil.logInfo("CloseBrowser : " + applicationURL)
		//		util.FW_TestStartStep(objectName)
		WebUI.closeBrowser()
		//		util.FW_TestEndStep()
	}

	@Keyword
	def void FW_WaitForPageLoad() {
		KeywordUtil.logInfo("FW_WaitForPageLoad : " + GlobalVariable.waitPresentTimeout)
		util.FW_TestStartStep("Page Loading")
		WebUI.waitForPageLoad(GlobalVariable.waitPresentTimeout)
		util.FW_TestEndStep()
	}

	@Keyword
	def void FW_WaitForPageLoad(int waitPresentTimeout) {
		KeywordUtil.logInfo("FW_WaitForPageLoad : " + waitPresentTimeout)
		util.FW_TestStartStep("Page Loading : " + waitPresentTimeout)
		WebUI.waitForPageLoad(waitPresentTimeout)
		util.FW_TestEndStep()
	}

	@Keyword
	def void FW_MaximizeWindow() {
		KeywordUtil.logInfo("FW_MaximizeWindow")
		//util.FW_TestStartStep("Maximize Size Window")
		WebUI.maximizeWindow()
		//		util.FW_CaptureScreenShot()
		//util.FW_TestEndStep()
	}

	@Keyword
	def void FW_WaitForElementVisible(TestObject testObject) {
		KeywordUtil.logInfo("FW_WaitForElementVisible")
		//util.FW_TestStartStep(objectName)
		WebUI.waitForElementVisible(testObject, GlobalVariable.waitPresentTimeout)
		//util.FW_TestEndStep()
	}

	@Keyword
	def void FW_WaitForElementVisible(TestObject testObject, String objectName, int waitPresentTimeout) {
		KeywordUtil.logInfo("FW_WaitForElementVisible")
		util.FW_TestStartStep(objectName)
		WebUI.waitForElementVisible(testObject, waitPresentTimeout)
		util.FW_TestEndStep()
	}

	@Keyword
	def void FW_WaitForElementPresent(TestObject testObject, String objectName) {
		KeywordUtil.logInfo("FW_WaitForElementPresent")
		util.FW_TestStartStep(objectName)
		WebUI.waitForElementPresent(testObject, GlobalVariable.waitPresentTimeout)
		util.FW_TestEndStep()
	}

	@Keyword
	def void FW_WaitForElementPresent(TestObject testObject, String objectName, int waitPresentTimeout) {
		KeywordUtil.logInfo("FW_WaitForElementPresent")
		util.FW_TestStartStep(objectName)
		WebUI.waitForElementPresent(testObject, waitPresentTimeout)
		util.FW_TestEndStep()
	}

	@Keyword
	def void FW_WaitForElementNotPresent(TestObject testObject, String objectName, int waitNotPresentTimeout) {
		KeywordUtil.logInfo("FW_WaitForElementNotPresent")
		util.FW_TestStartStep(objectName)
		WebUI.waitForElementNotPresent(testObject, waitNotPresentTimeout)
		util.FW_TestEndStep()
	}

	@Keyword
	def void FW_VerifyElementPresent(TestObject testObject) {
		KeywordUtil.logInfo("FW_VerifyElementPresent")
		//		util.FW_TestStartStep(objectName)
		WebUI.verifyElementPresent(testObject, GlobalVariable.waitPresentTimeout)
		//		util.FW_TestEndStep()
	}

	@Keyword
	def void FW_VerifyElementPresent(TestObject testObject, String objectName, int waitPresentTimeout) {
		KeywordUtil.logInfo("FW_VerifyElementPresent")
		util.FW_TestStartStep(objectName)
		WebUI.verifyElementPresent(testObject, waitPresentTimeout)
		util.FW_TestEndStep()
	}


	@Keyword
	def void FW_VerifyElementVisible(TestObject testObject) {
		KeywordUtil.logInfo("FW_VerifyElementPresent")
		//		util.FW_TestStartStep(objectName)
		WebUI.verifyElementVisible(testObject)

		//		util.FW_TestEndStep()
	}


	@Keyword
	def void FW_SetText(TestObject testObject, String MsgDescaption,String value) {


		try {
			KeywordUtil.logInfo("FW_SetText")
			WebUI.setText(testObject, value, FailureHandling.STOP_ON_FAILURE)
			KeywordUtil.logInfo("SetText : " + value)
			KeywordUtil.logInfo("FW_SetText Passed")
		}catch (StepFailedException ex) {
			GlobalVariable.MsgErr	= "ไม่สามารถ" + MsgDescaption
			KeywordUtil.logInfo("Msg error :" + GlobalVariable.MsgErr)
			util.FW_CaptureScreenShot()
			util.FW_TestEndStep()
			KeywordUtil.markFailedAndStop("FW_SetText failed")
		}
		//		KeywordUtil.logInfo("FW_SetText")
		//		util.FW_TestStartStep(objectName, value)
		//		WebUI.setText(testObject, value)
		//		util.FW_CaptureScreenShot()
		//		util.FW_TestEndStep()
	}

	@Keyword
	def void FW_SendKeys(TestObject testObject, String objectName, String value) {
		KeywordUtil.logInfo("FW_SendKeys")
		util.FW_TestStartStep(objectName, value)
		WebUI.sendKeys(testObject, value)
		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()
	}

	@Keyword
	def void FW_Click(TestObject testObject, String MsgDescaption) {
		//		KeywordUtil.logInfo("FW_Click")
		//		//util.FW_TestStartStep(objectName)
		//		WebUI.click(testObject)
		//		util.FW_CaptureScreenShot()
		//		///util.FW_TestEndStep()

		try {
			KeywordUtil.logInfo("FW_Click")
			WebUI.verifyElementVisible(testObject)
			WebUI.click(testObject, FailureHandling.STOP_ON_FAILURE)
			KeywordUtil.logInfo("FW_Click Passed")
		}
		catch (StepFailedException ex) {
			GlobalVariable.MsgErr	= "ไม่สามารถ" + MsgDescaption
			KeywordUtil.logInfo("Msg error :" + GlobalVariable.MsgErr)
			util.FW_CaptureScreenShot()
			util.FW_TestEndStep()
			KeywordUtil.markFailedAndStop("FW_Click failed")
		}
	}

	@Keyword
	def void FW_Delay(int delayTime) {
		KeywordUtil.logInfo("FW_Delay")
		//		util.FW_TestStartStep("Delay", "")
		WebUI.delay(delayTime)
		//		util.FW_TestEndStep()
	}

	@Keyword
	def void FW_SelectOptionByValue(TestObject testObject, String MsgDescaption,String value) {
		//		KeywordUtil.logInfo("FW_SelectOptionByValue")
		//		//		util.FW_TestStartStep("SelectOptionByValue", objectName + " : " + value)
		//		WebUI.selectOptionByValue(testObject, value, true)
		//		util.FW_CaptureScreenShot()
		//		//		util.FW_TestEndStep()
		try {
			KeywordUtil.logInfo("FW_SelectOptionByValue")
			WebUI.selectOptionByValue(testObject, value, true)
			KeywordUtil.logInfo("SelectOptionByValue :" + value )
			KeywordUtil.logInfo("FW_SelectOptionByValue Passed")

		}
		catch (StepFailedException ex) {
			GlobalVariable.MsgErr	= " Element Not found " + testObject
			KeywordUtil.logInfo("Msg error :" + GlobalVariable.MsgErr)
			util.FW_CaptureScreenShot()
			util.FW_TestEndStep()
			KeywordUtil.markFailedAndStop("FW_SelectOptionByValue failed")
		}

	}
	@Keyword
	def void FW_CheckMessage(TestObject testObject,String MsgDescaption, String ExpectedValue) {

		String ActualValue
		try {
			KeywordUtil.logInfo("FW_CheckMessage")
			ActualValue =  WebUI.getText(testObject)
			KeywordUtil.logInfo("Display show Message :"+ ActualValue)
			WebUI.verifyEqual(ActualValue, ExpectedValue, FailureHandling.STOP_ON_FAILURE)
			//			WebUI.verifyElementText(testObject, ExpectedValue,  FailureHandling.STOP_ON_FAILURE)
			KeywordUtil.logInfo("CheckMessage Passed :"+ ActualValue +"="+ExpectedValue)
		}catch (StepFailedException ex) {
			GlobalVariable.MsgErr	= "ข้อมูลที่ส่งเข้ามา"  + ActualValue +"ไม่ตรงกับ" + ExpectedValue
			KeywordUtil.logInfo(GlobalVariable.MsgErr)
			util.FW_CaptureScreenShot()
			util.FW_TestEndStep()
			KeywordUtil.markFailedAndStop("FW_CheckMessage failed")

		}






		//		catch (WebElementNotFoundException webex) {
		//			GlobalVariable.MsgErr	= "ไม่สามารถ" + MsgDescaption
		//			KeywordUtil.logInfo("Msg error :" + GlobalVariable.MsgErr)
		//			util.FW_CaptureScreenShot()
		//			util.FW_TestEndStep()
		//			KeywordUtil.markFailedAndStop("FW_CheckMessage failed")
		//		}

		//		util.FW_TestStartStep("Check Message", objectName + " : " + ExpectedValue)

		//		String ActualValue =  WebUI.getText(testObject)
		//		WebUI.verifyEqual(ActualValue, ExpectedValue)


		//		util.FW_TestEndStep()
	}

	@Keyword
	def void FW_CheckMessageBygetAttribute(TestObject testObject,String MsgDescaption, String ExpectedValue) {

		String ActualValue
		try {
			KeywordUtil.logInfo("FW_CheckMessage")
			ActualValue =  WebUI.getAttribute(testObject, "value")
			KeywordUtil.logInfo("Display show Message :"+ ActualValue)
			WebUI.verifyEqual(ActualValue, ExpectedValue, FailureHandling.STOP_ON_FAILURE)
			//			WebUI.verifyElementText(testObject, ExpectedValue,  FailureHandling.STOP_ON_FAILURE)
			KeywordUtil.logInfo("CheckMessage Passed :"+ ActualValue +"="+ExpectedValue)
		}catch (StepFailedException ex) {
			GlobalVariable.MsgErr	= "ข้อมูลที่ส่งเข้ามา"  + ActualValue +"ไม่ตรงกับ" + ExpectedValue
			KeywordUtil.logInfo(GlobalVariable.MsgErr)
			util.FW_CaptureScreenShot()
			util.FW_TestEndStep()
			KeywordUtil.markFailedAndStop("FW_CheckMessage failed")

		}



	}
	
	@Keyword
	def void FW_CheckMessageBygetAttributeName(TestObject testObject,String MsgDescaption, String ExpectedValue) {

		String ActualValue
		try {
			KeywordUtil.logInfo("FW_CheckMessage")
			ActualValue =  WebUI.getAttribute(testObject, "name")
			KeywordUtil.logInfo("Display show Message :"+ ActualValue)
			WebUI.verifyEqual(ActualValue, ExpectedValue, FailureHandling.STOP_ON_FAILURE)
			//			WebUI.verifyElementText(testObject, ExpectedValue,  FailureHandling.STOP_ON_FAILURE)
			KeywordUtil.logInfo("CheckMessage Passed :"+ ActualValue +"="+ExpectedValue)
		}catch (StepFailedException ex) {
			GlobalVariable.MsgErr	= "ข้อมูลที่ส่งเข้ามา"  + ActualValue +"ไม่ตรงกับ" + ExpectedValue
			KeywordUtil.logInfo(GlobalVariable.MsgErr)
			util.FW_CaptureScreenShot()
			util.FW_TestEndStep()
			KeywordUtil.markFailedAndStop("FW_CheckMessage failed")

		}



	}

	@Keyword
	def void FW_CheckMessageByJS(String JSActualValue, String MsgDescaption, String ExpectedValue) {

		String ActualValue

		try {
			ActualValue = WebUI.executeJavaScript(JSActualValue, ['null'])
			//		WebUI.verifyElementText(testObject, ExpectedValue)
			//		String ActualValue =  WebUI.getText(testObject)
			KeywordUtil.logInfo("Display show Message :  "+ ActualValue)
			WebUI.verifyEqual(ExpectedValue, ActualValue)
			KeywordUtil.logInfo("CheckMessage Passed : "+ ActualValue +" = "+ExpectedValue)
		}
		catch (StepFailedException ex) {
			GlobalVariable.MsgErr	= "ข้อมูลที่ส่งเข้ามา"  + ActualValue +"ไม่ตรงกับ" + ExpectedValue
			KeywordUtil.logInfo(GlobalVariable.MsgErr)

			util.FW_CaptureScreenShot()
			util.FW_TestEndStep()
			KeywordUtil.markFailedAndStop("FW_CheckMessage failed")


		}
		//		KeywordUtil.logInfo("FW_CheckMessage")
		//		//		util.FW_TestStartStep("Check Message", objectName + " : " + ExpectedValue)
		//
		//		String ActualValue = WebUI.executeJavaScript(JSActualValue, ['null'])
		//		//		WebUI.verifyElementText(testObject, ExpectedValue)
		//		//		String ActualValue =  WebUI.getText(testObject)
		//		WebUI.verifyEqual(ExpectedValue, ActualValue)
		//		KeywordUtil.logInfo("CheckMessage Pass")

		//		util.FW_TestEndStep()
	}


	/**
	 * Refresh browser
	 */
	@Keyword
	def refreshBrowser() {
		KeywordUtil.logInfo("Refreshing")
		WebDriver webDriver = DriverFactory.getWebDriver()
		webDriver.navigate().refresh()
		KeywordUtil.markPassed("Refresh successfully")
	}

	/**
	 * Click element
	 * @param to Katalon test object
	 */
	@Keyword
	def clickElement(TestObject to) {
		try {
			WebElement element = WebUiBuiltInKeywords.findWebElement(to);
			KeywordUtil.logInfo("Clicking element")
			element.click()
			KeywordUtil.markPassed("Element has been clicked")
		} catch (WebElementNotFoundException e) {
			KeywordUtil.markFailed("Element not found")
		} catch (Exception e) {
			KeywordUtil.markFailed("Fail to click on element")
		}
	}

	/**
	 * Get all rows of HTML table
	 * @param table Katalon test object represent for HTML table
	 * @param outerTagName outer tag name of TR tag, usually is TBODY
	 * @return All rows inside HTML table
	 */
	@Keyword
	def List<WebElement> getHtmlTableRows(TestObject table, String outerTagName) {
		WebElement mailList = WebUiBuiltInKeywords.findWebElement(table)
		List<WebElement> selectedRows = mailList.findElements(By.xpath("./" + outerTagName + "/tr"))
		return selectedRows
	}

	/**
	 * Refresh browser
	 */
	@Keyword
	def FW_WaitForElementPresent(TestObject testObject, int waitPresentTimeout) {
		KeywordUtil.logInfo("FW_WaitForElementPresent")
		util.FW_TestStartStep("FW_WaitForElementPresent", "")
		WebUI.waitForElementPresent(testObject, waitPresentTimeout)
		util.FW_TestEndStep()
	}
}