package borrowingpowercalculator

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.extosoft.keyword.UtilityKeyword
import com.extosoft.keyword.WebKeyword
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable

public class BorrowingPowerCalculator {

	def WebKeyword web = new WebKeyword()
	def UtilityKeyword util = new UtilityKeyword()

	@Keyword
	def void LunchWebKrungsriBorrowingPower(){
		util.FW_TestStartStep("เปิดเว็บกรุงศรีความสามารถในการกู้")
		///**************
		web.FW_OpenBrowser("https://www.krungsri.com/bank/th/Other/Calculator/Loancalculator/BorrowingPowerCalculator.html")
		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()
	}
	@Keyword
	def void InputLoanPersonIncome(){
		util.FW_TestStartStep("กรอกราคาค่างวดต่อเดือน")
		///**************
		web.FW_Click(findTestObject("Object Repository/Page_/a_"), "close pop up")
		web.FW_SetText(findTestObject('Object Repository/BorrowingPowerCalculator/Page_/input__pltctl19ksPpltctl03CalBorrowtxtLoanPersonIncome'), "หน้าจอแสดงค่างวดต่อเดือน “30,000 บาท”", findTestData('TC005').getValue(1, 1))
		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()
	}

	@Keyword
	def void InputExpensePeriodAmount(){
		util.FW_TestStartStep("กรอกราคาค่าใช้จ่ายต่อเดือน")
		///**************
		web.FW_SetText(findTestObject('Object Repository/BorrowingPowerCalculator/Page_/input__pltctl19ksPpltctl03CalBorrowtxtExpensePeriodAmount'), "หน้าจอแสดงค่าใช้จ่ายต่อเดือน “15,000 บาท”", findTestData('TC005').getValue(2, 1))
		//findTestObject('Object Repository/BorrowingPowerCalculator/Page_/input__pltctl19ksPpltctl03CalBorrowtxtExpensePeriodAmount')
		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()
	}

	@Keyword
	def void InputInterestRate(){
		util.FW_TestStartStep("ดอกเบี้ยที่ใช้ในการคำนวณ")
		///**************
		web.FW_SetText(findTestObject('Object Repository/BorrowingPowerCalculator/Page_/input__pltctl19ksPpltctl03CalBorrowtxtInterestRate'), "หน้าจอแสดงดอกเบี้ยที่ใช้ในการคำนวณ “5 %”", findTestData('TC005').getValue(3, 1))
		//findTefindTestObject('Object Repository/BorrowingPowerCalculator/Page_/input__pltctl19ksPpltctl03CalBorrowtxtInterestRate')
		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()
	}

	@Keyword
	def void InputLoanTotalYear(){
		util.FW_TestStartStep("ระยะเวลาที่ขอกู้ (ปี)")
		///**************
		web.FW_SetText(findTestObject('Object Repository/BorrowingPowerCalculator/Page_/input__pltctl19ksPpltctl03CalBorrowtxtLoanTotalYear'), "หน้าจอแสดงระยะเวลาที่ขอกู้   “5 %”", findTestData('TC005').getValue(4, 1))
		//findTestObject('Object Repository/BorrowingPowerCalculator/Page_/input__pltctl19ksPpltctl03CalBorrowtxtLoanTotalYear')
		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()
	}

	@Keyword
	def void ClickCalBorrowbtnCalculate(){
		///**************
		util.FW_TestStartStep("กดปุ่มเริ่มคำนวณ")
		web.FW_Click(findTestObject('Object Repository/BorrowingPowerCalculator/Page_/input_SMECalBorrowLabellblAlert10_pltctl19ksPpltctl03CalBorrowbtnCalculate'), "หน้าจอแสดงหน้าจอแสดงผลการคำนวณ”")
		//findTestObject('Object Repository/BorrowingPowerCalculator/Page_/input_SMECalBorrowLabellblAlert10_pltctl19ksPpltctl03CalBorrowbtnCalculate')
		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()
	}

	@Keyword
	def void VerifyYoucanrecover(){
		util.FW_TestStartStep("ตรวจสอบท่านสามารถกู้ได้")
		WebUI.delay(10)
		web.FW_CheckMessage(findTestObject('Object Repository/BorrowingPowerCalculator/Page_/TotalApprovableAmount'), "หน้าจอแสดงท่านสามารถกู้ได้   “232,800.00”", findTestData('TC005').getValue(9, 1))
		//		web.FW_CheckMessage(findTestObject('Object Repository/BorrowingPowerCalculator/Page_/TotalApprovableAmount'), "232,800.00","232,800.00")
		//findTestObject('Object Repository/BorrowingPowerCalculator/Page_/span_23280000')
		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()
	}

	@Keyword
	def void VerifyrMonthlyinstallment(){
		util.FW_TestStartStep("ตรวจสอบค่างวดต่อเดือน")
		web.FW_CheckMessage(findTestObject('Object Repository/BorrowingPowerCalculator/Page_/PeriodAmount'), "หน้าจอแสดงค่างวดต่อเดือน  “4,500.00”", findTestData('TC005').getValue(10, 1))
		//web.FW_CheckMessage(findTestObject('Object Repository/BorrowingPowerCalculator/Page_/span_450000'), "4,500.00","4,500.00")
		//findTestObject('Object Repository/BorrowingPowerCalculator/Page_/span_450000')
		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()
	}


	@Keyword

	def void CloseWebKrungsriBorrowingPower(){
		util.FW_TestStartStep("ปิดเว็บกรุงศรีความสามารถในการกู้")
		
		///**************
		web.FW_CloseBrowser("https://www.krungsri.com/bank/th/Other/Calculator/Loancalculator/BorrowingPowerCalculator.html")
		
		util.FW_TestEndStep()
	}
}
