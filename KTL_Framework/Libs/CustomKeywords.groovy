
/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */

import java.lang.String

import com.kms.katalon.core.testobject.TestObject

import org.apache.poi.ss.usermodel.Workbook

import java.util.List

import org.apache.poi.ss.usermodel.Sheet

import java.util.Map

import org.apache.poi.ss.usermodel.Cell

import org.apache.poi.ss.usermodel.Row


def static "krungsriauto.CalculateLoan.LunchWebKrungsriauto"() {
    (new krungsriauto.CalculateLoan()).LunchWebKrungsriauto()
}

def static "krungsriauto.CalculateLoan.ClickPreliminaryCreditCalculation"() {
    (new krungsriauto.CalculateLoan()).ClickPreliminaryCreditCalculation()
}

def static "krungsriauto.CalculateLoan.SelectLoanType"() {
    (new krungsriauto.CalculateLoan()).SelectLoanType()
}

def static "krungsriauto.CalculateLoan.InputCarprice"() {
    (new krungsriauto.CalculateLoan()).InputCarprice()
}

def static "krungsriauto.CalculateLoan.InputDownPayment"() {
    (new krungsriauto.CalculateLoan()).InputDownPayment()
}

def static "krungsriauto.CalculateLoan.SelectInstallmentPeriod"() {
    (new krungsriauto.CalculateLoan()).SelectInstallmentPeriod()
}

def static "krungsriauto.CalculateLoan.InterestRatePerMonth"() {
    (new krungsriauto.CalculateLoan()).InterestRatePerMonth()
}

def static "krungsriauto.CalculateLoan.ClickCalculateloan"() {
    (new krungsriauto.CalculateLoan()).ClickCalculateloan()
}

def static "krungsriauto.CalculateLoan.VerifyInterestRatePerMonth"() {
    (new krungsriauto.CalculateLoan()).VerifyInterestRatePerMonth()
}

def static "krungsriauto.CalculateLoan.CloseCalculateLoanWebbrowser"() {
    (new krungsriauto.CalculateLoan()).CloseCalculateLoanWebbrowser()
}

def static "krungsrimarket.CalculateLoan.LunchWebKrungsrimarket"() {
    (new krungsrimarket.CalculateLoan()).LunchWebKrungsrimarket()
}

def static "krungsrimarket.CalculateLoan.SelectCarMakeId"() {
    (new krungsrimarket.CalculateLoan()).SelectCarMakeId()
}

def static "krungsrimarket.CalculateLoan.SelectCarTypeId"() {
    (new krungsrimarket.CalculateLoan()).SelectCarTypeId()
}

def static "krungsrimarket.CalculateLoan.SelectCarModelId"() {
    (new krungsrimarket.CalculateLoan()).SelectCarModelId()
}

def static "krungsrimarket.CalculateLoan.SelectYear"() {
    (new krungsrimarket.CalculateLoan()).SelectYear()
}

def static "krungsrimarket.CalculateLoan.InputCarPrince"() {
    (new krungsrimarket.CalculateLoan()).InputCarPrince()
}

def static "krungsrimarket.CalculateLoan.InputDownPaymentPercent"() {
    (new krungsrimarket.CalculateLoan()).InputDownPaymentPercent()
}

def static "krungsrimarket.CalculateLoan.CheckTotalDawn"() {
    (new krungsrimarket.CalculateLoan()).CheckTotalDawn()
}

def static "krungsrimarket.CalculateLoan.CheckHirePurchaseSales"() {
    (new krungsrimarket.CalculateLoan()).CheckHirePurchaseSales()
}

def static "krungsrimarket.CalculateLoan.ClickCalculatelLoan"() {
    (new krungsrimarket.CalculateLoan()).ClickCalculatelLoan()
}

def static "krungsrimarket.CalculateLoan.ClickCalculatedFromMonthlyTabMenu"() {
    (new krungsrimarket.CalculateLoan()).ClickCalculatedFromMonthlyTabMenu()
}

def static "krungsrimarket.CalculateLoan.InputPeriodPayValue"() {
    (new krungsrimarket.CalculateLoan()).InputPeriodPayValue()
}

def static "krungsrimarket.CalculateLoan.InputDownPaymentValue"() {
    (new krungsrimarket.CalculateLoan()).InputDownPaymentValue()
}

def static "krungsrimarket.CalculateLoan.InputRateCutOFF"() {
    (new krungsrimarket.CalculateLoan()).InputRateCutOFF()
}

def static "krungsrimarket.CalculateLoan.ClickButtonCalculateLoanMonthlyInstallments"() {
    (new krungsrimarket.CalculateLoan()).ClickButtonCalculateLoanMonthlyInstallments()
}

def static "krungsrimarket.CalculateLoan.ClickCalculateConditionalCreditsTAB"() {
    (new krungsrimarket.CalculateLoan()).ClickCalculateConditionalCreditsTAB()
}

def static "krungsrimarket.CalculateLoan.InputCarPriceConditionalCredits"() {
    (new krungsrimarket.CalculateLoan()).InputCarPriceConditionalCredits()
}

def static "krungsrimarket.CalculateLoan.InputDownPaymentPercentConditionalCredits"() {
    (new krungsrimarket.CalculateLoan()).InputDownPaymentPercentConditionalCredits()
}

def static "krungsrimarket.CalculateLoan.VerifyDownPaymentConditionalCredits"() {
    (new krungsrimarket.CalculateLoan()).VerifyDownPaymentConditionalCredits()
}

def static "krungsrimarket.CalculateLoan.VerifyPurchase"() {
    (new krungsrimarket.CalculateLoan()).VerifyPurchase()
}

def static "krungsrimarket.CalculateLoan.InputRateCutOffConditionalCredits"() {
    (new krungsrimarket.CalculateLoan()).InputRateCutOffConditionalCredits()
}

def static "krungsrimarket.CalculateLoan.ClickButtonCalculateLoanConditionalCredits"() {
    (new krungsrimarket.CalculateLoan()).ClickButtonCalculateLoanConditionalCredits()
}

def static "krungsrimarket.CalculateLoan.CloseCalculateLoanWebbrowser"() {
    (new krungsrimarket.CalculateLoan()).CloseCalculateLoanWebbrowser()
}

def static "borrowingpowercalculator.BorrowingPowerCalculator.LunchWebKrungsriBorrowingPower"() {
    (new borrowingpowercalculator.BorrowingPowerCalculator()).LunchWebKrungsriBorrowingPower()
}

def static "borrowingpowercalculator.BorrowingPowerCalculator.InputLoanPersonIncome"() {
    (new borrowingpowercalculator.BorrowingPowerCalculator()).InputLoanPersonIncome()
}

def static "borrowingpowercalculator.BorrowingPowerCalculator.InputExpensePeriodAmount"() {
    (new borrowingpowercalculator.BorrowingPowerCalculator()).InputExpensePeriodAmount()
}

def static "borrowingpowercalculator.BorrowingPowerCalculator.InputInterestRate"() {
    (new borrowingpowercalculator.BorrowingPowerCalculator()).InputInterestRate()
}

def static "borrowingpowercalculator.BorrowingPowerCalculator.InputLoanTotalYear"() {
    (new borrowingpowercalculator.BorrowingPowerCalculator()).InputLoanTotalYear()
}

def static "borrowingpowercalculator.BorrowingPowerCalculator.ClickCalBorrowbtnCalculate"() {
    (new borrowingpowercalculator.BorrowingPowerCalculator()).ClickCalBorrowbtnCalculate()
}

def static "borrowingpowercalculator.BorrowingPowerCalculator.VerifyYoucanrecover"() {
    (new borrowingpowercalculator.BorrowingPowerCalculator()).VerifyYoucanrecover()
}

def static "borrowingpowercalculator.BorrowingPowerCalculator.VerifyrMonthlyinstallment"() {
    (new borrowingpowercalculator.BorrowingPowerCalculator()).VerifyrMonthlyinstallment()
}

def static "borrowingpowercalculator.BorrowingPowerCalculator.CloseWebKrungsriBorrowingPower"() {
    (new borrowingpowercalculator.BorrowingPowerCalculator()).CloseWebKrungsriBorrowingPower()
}

def static "com.extosoft.keyword.WebKeyword.FW_OpenBrowser"(
    	String applicationURL	) {
    (new com.extosoft.keyword.WebKeyword()).FW_OpenBrowser(
        	applicationURL)
}

def static "com.extosoft.keyword.WebKeyword.FW_CloseBrowser"(
    	String applicationURL	) {
    (new com.extosoft.keyword.WebKeyword()).FW_CloseBrowser(
        	applicationURL)
}

def static "com.extosoft.keyword.WebKeyword.FW_WaitForPageLoad"() {
    (new com.extosoft.keyword.WebKeyword()).FW_WaitForPageLoad()
}

def static "com.extosoft.keyword.WebKeyword.FW_WaitForPageLoad"(
    	int waitPresentTimeout	) {
    (new com.extosoft.keyword.WebKeyword()).FW_WaitForPageLoad(
        	waitPresentTimeout)
}

def static "com.extosoft.keyword.WebKeyword.FW_MaximizeWindow"() {
    (new com.extosoft.keyword.WebKeyword()).FW_MaximizeWindow()
}

def static "com.extosoft.keyword.WebKeyword.FW_WaitForElementVisible"(
    	TestObject testObject	) {
    (new com.extosoft.keyword.WebKeyword()).FW_WaitForElementVisible(
        	testObject)
}

def static "com.extosoft.keyword.WebKeyword.FW_WaitForElementVisible"(
    	TestObject testObject	
     , 	String objectName	
     , 	int waitPresentTimeout	) {
    (new com.extosoft.keyword.WebKeyword()).FW_WaitForElementVisible(
        	testObject
         , 	objectName
         , 	waitPresentTimeout)
}

def static "com.extosoft.keyword.WebKeyword.FW_WaitForElementPresent"(
    	TestObject testObject	
     , 	String objectName	) {
    (new com.extosoft.keyword.WebKeyword()).FW_WaitForElementPresent(
        	testObject
         , 	objectName)
}

def static "com.extosoft.keyword.WebKeyword.FW_WaitForElementPresent"(
    	TestObject testObject	
     , 	String objectName	
     , 	int waitPresentTimeout	) {
    (new com.extosoft.keyword.WebKeyword()).FW_WaitForElementPresent(
        	testObject
         , 	objectName
         , 	waitPresentTimeout)
}

def static "com.extosoft.keyword.WebKeyword.FW_WaitForElementNotPresent"(
    	TestObject testObject	
     , 	String objectName	
     , 	int waitNotPresentTimeout	) {
    (new com.extosoft.keyword.WebKeyword()).FW_WaitForElementNotPresent(
        	testObject
         , 	objectName
         , 	waitNotPresentTimeout)
}

def static "com.extosoft.keyword.WebKeyword.FW_VerifyElementPresent"(
    	TestObject testObject	) {
    (new com.extosoft.keyword.WebKeyword()).FW_VerifyElementPresent(
        	testObject)
}

def static "com.extosoft.keyword.WebKeyword.FW_VerifyElementPresent"(
    	TestObject testObject	
     , 	String objectName	
     , 	int waitPresentTimeout	) {
    (new com.extosoft.keyword.WebKeyword()).FW_VerifyElementPresent(
        	testObject
         , 	objectName
         , 	waitPresentTimeout)
}

def static "com.extosoft.keyword.WebKeyword.FW_VerifyElementVisible"(
    	TestObject testObject	) {
    (new com.extosoft.keyword.WebKeyword()).FW_VerifyElementVisible(
        	testObject)
}

def static "com.extosoft.keyword.WebKeyword.FW_SetText"(
    	TestObject testObject	
     , 	String MsgDescaption	
     , 	String value	) {
    (new com.extosoft.keyword.WebKeyword()).FW_SetText(
        	testObject
         , 	MsgDescaption
         , 	value)
}

def static "com.extosoft.keyword.WebKeyword.FW_SendKeys"(
    	TestObject testObject	
     , 	String objectName	
     , 	String value	) {
    (new com.extosoft.keyword.WebKeyword()).FW_SendKeys(
        	testObject
         , 	objectName
         , 	value)
}

def static "com.extosoft.keyword.WebKeyword.FW_Click"(
    	TestObject testObject	
     , 	String MsgDescaption	) {
    (new com.extosoft.keyword.WebKeyword()).FW_Click(
        	testObject
         , 	MsgDescaption)
}

def static "com.extosoft.keyword.WebKeyword.FW_Delay"(
    	int delayTime	) {
    (new com.extosoft.keyword.WebKeyword()).FW_Delay(
        	delayTime)
}

def static "com.extosoft.keyword.WebKeyword.FW_SelectOptionByValue"(
    	TestObject testObject	
     , 	String MsgDescaption	
     , 	String value	) {
    (new com.extosoft.keyword.WebKeyword()).FW_SelectOptionByValue(
        	testObject
         , 	MsgDescaption
         , 	value)
}

def static "com.extosoft.keyword.WebKeyword.FW_CheckMessage"(
    	TestObject testObject	
     , 	String MsgDescaption	
     , 	String ExpectedValue	) {
    (new com.extosoft.keyword.WebKeyword()).FW_CheckMessage(
        	testObject
         , 	MsgDescaption
         , 	ExpectedValue)
}

def static "com.extosoft.keyword.WebKeyword.FW_CheckMessageBygetAttribute"(
    	TestObject testObject	
     , 	String MsgDescaption	
     , 	String ExpectedValue	) {
    (new com.extosoft.keyword.WebKeyword()).FW_CheckMessageBygetAttribute(
        	testObject
         , 	MsgDescaption
         , 	ExpectedValue)
}

def static "com.extosoft.keyword.WebKeyword.FW_CheckMessageBygetAttributeName"(
    	TestObject testObject	
     , 	String MsgDescaption	
     , 	String ExpectedValue	) {
    (new com.extosoft.keyword.WebKeyword()).FW_CheckMessageBygetAttributeName(
        	testObject
         , 	MsgDescaption
         , 	ExpectedValue)
}

def static "com.extosoft.keyword.WebKeyword.FW_CheckMessageByJS"(
    	String JSActualValue	
     , 	String MsgDescaption	
     , 	String ExpectedValue	) {
    (new com.extosoft.keyword.WebKeyword()).FW_CheckMessageByJS(
        	JSActualValue
         , 	MsgDescaption
         , 	ExpectedValue)
}

def static "com.extosoft.keyword.WebKeyword.refreshBrowser"() {
    (new com.extosoft.keyword.WebKeyword()).refreshBrowser()
}

def static "com.extosoft.keyword.WebKeyword.clickElement"(
    	TestObject to	) {
    (new com.extosoft.keyword.WebKeyword()).clickElement(
        	to)
}

def static "com.extosoft.keyword.WebKeyword.getHtmlTableRows"(
    	TestObject table	
     , 	String outerTagName	) {
    (new com.extosoft.keyword.WebKeyword()).getHtmlTableRows(
        	table
         , 	outerTagName)
}

def static "com.extosoft.keyword.WebKeyword.FW_WaitForElementPresent"(
    	TestObject testObject	
     , 	int waitPresentTimeout	) {
    (new com.extosoft.keyword.WebKeyword()).FW_WaitForElementPresent(
        	testObject
         , 	waitPresentTimeout)
}

def static "com.extosoft.keyword.ExcelKeyword.createExcelFile"(
    	String filePath	) {
    (new com.extosoft.keyword.ExcelKeyword()).createExcelFile(
        	filePath)
}

def static "com.extosoft.keyword.ExcelKeyword.createExcelSheet"(
    	Workbook workbook	) {
    (new com.extosoft.keyword.ExcelKeyword()).createExcelSheet(
        	workbook)
}

def static "com.extosoft.keyword.ExcelKeyword.createExcelSheet"(
    	Workbook workbook	
     , 	String sheetName	) {
    (new com.extosoft.keyword.ExcelKeyword()).createExcelSheet(
        	workbook
         , 	sheetName)
}

def static "com.extosoft.keyword.ExcelKeyword.getSheetNames"(
    	Workbook workbook	) {
    (new com.extosoft.keyword.ExcelKeyword()).getSheetNames(
        	workbook)
}

def static "com.extosoft.keyword.ExcelKeyword.createExcelSheets"(
    	Workbook workbook	
     , 	java.util.List<String> sheetNames	) {
    (new com.extosoft.keyword.ExcelKeyword()).createExcelSheets(
        	workbook
         , 	sheetNames)
}

def static "com.extosoft.keyword.ExcelKeyword.getWorkbook"(
    	String filePath	) {
    (new com.extosoft.keyword.ExcelKeyword()).getWorkbook(
        	filePath)
}

def static "com.extosoft.keyword.ExcelKeyword.createWorkbook"(
    	String filePath	) {
    (new com.extosoft.keyword.ExcelKeyword()).createWorkbook(
        	filePath)
}

def static "com.extosoft.keyword.ExcelKeyword.saveWorkbook"(
    	String filePath	
     , 	Workbook workbook	) {
    (new com.extosoft.keyword.ExcelKeyword()).saveWorkbook(
        	filePath
         , 	workbook)
}

def static "com.extosoft.keyword.ExcelKeyword.setValueToCellByIndex"(
    	Sheet sheet	
     , 	int rowIndex	
     , 	int colIndex	
     , 	Object value	) {
    (new com.extosoft.keyword.ExcelKeyword()).setValueToCellByIndex(
        	sheet
         , 	rowIndex
         , 	colIndex
         , 	value)
}

def static "com.extosoft.keyword.ExcelKeyword.setValueToCellByAddress"(
    	Sheet sheet	
     , 	String cellAddress	
     , 	Object value	) {
    (new com.extosoft.keyword.ExcelKeyword()).setValueToCellByAddress(
        	sheet
         , 	cellAddress
         , 	value)
}

def static "com.extosoft.keyword.ExcelKeyword.setValueToCellByAddresses"(
    	Sheet sheet	
     , 	Map content	) {
    (new com.extosoft.keyword.ExcelKeyword()).setValueToCellByAddresses(
        	sheet
         , 	content)
}

def static "com.extosoft.keyword.ExcelKeyword.getExcelSheet"(
    	String filePath	
     , 	int sheetIndex	) {
    (new com.extosoft.keyword.ExcelKeyword()).getExcelSheet(
        	filePath
         , 	sheetIndex)
}

def static "com.extosoft.keyword.ExcelKeyword.getExcelSheet"(
    	Workbook wbs	
     , 	String sheetName	) {
    (new com.extosoft.keyword.ExcelKeyword()).getExcelSheet(
        	wbs
         , 	sheetName)
}

def static "com.extosoft.keyword.ExcelKeyword.getExcelSheetByName"(
    	String filePath	
     , 	String sheetName	) {
    (new com.extosoft.keyword.ExcelKeyword()).getExcelSheetByName(
        	filePath
         , 	sheetName)
}

def static "com.extosoft.keyword.ExcelKeyword.getCellByIndex"(
    	Sheet sheet	
     , 	int rowIdx	
     , 	int colIdx	) {
    (new com.extosoft.keyword.ExcelKeyword()).getCellByIndex(
        	sheet
         , 	rowIdx
         , 	colIdx)
}

def static "com.extosoft.keyword.ExcelKeyword.getCellByAddress"(
    	Sheet sheet	
     , 	String cellAddress	) {
    (new com.extosoft.keyword.ExcelKeyword()).getCellByAddress(
        	sheet
         , 	cellAddress)
}

def static "com.extosoft.keyword.ExcelKeyword.locateCell"(
    	Sheet sheet	
     , 	Object cellContent	) {
    (new com.extosoft.keyword.ExcelKeyword()).locateCell(
        	sheet
         , 	cellContent)
}

def static "com.extosoft.keyword.ExcelKeyword.getCellValue"(
    	Cell cell	) {
    (new com.extosoft.keyword.ExcelKeyword()).getCellValue(
        	cell)
}

def static "com.extosoft.keyword.ExcelKeyword.getCellValueByIndex"(
    	Sheet sheet	
     , 	int rowIdx	
     , 	int colIdx	) {
    (new com.extosoft.keyword.ExcelKeyword()).getCellValueByIndex(
        	sheet
         , 	rowIdx
         , 	colIdx)
}

def static "com.extosoft.keyword.ExcelKeyword.getCellIndexByAddress"(
    	Sheet sheet	
     , 	String cellAddress	) {
    (new com.extosoft.keyword.ExcelKeyword()).getCellIndexByAddress(
        	sheet
         , 	cellAddress)
}

def static "com.extosoft.keyword.ExcelKeyword.getCellValueByAddress"(
    	Sheet sheet	
     , 	String cellAddress	) {
    (new com.extosoft.keyword.ExcelKeyword()).getCellValueByAddress(
        	sheet
         , 	cellAddress)
}

def static "com.extosoft.keyword.ExcelKeyword.getRowIndexByCellContent"(
    	Sheet sheet	
     , 	String cellContent	
     , 	int columnIdxForCell	) {
    (new com.extosoft.keyword.ExcelKeyword()).getRowIndexByCellContent(
        	sheet
         , 	cellContent
         , 	columnIdxForCell)
}

def static "com.extosoft.keyword.ExcelKeyword.getTableContent"(
    	Sheet sheet	
     , 	int startRow	
     , 	int endRow	) {
    (new com.extosoft.keyword.ExcelKeyword()).getTableContent(
        	sheet
         , 	startRow
         , 	endRow)
}

def static "com.extosoft.keyword.ExcelKeyword.getDataRows"(
    	Sheet sheet	
     , 	java.util.List<java.lang.Integer> rowIndexs	) {
    (new com.extosoft.keyword.ExcelKeyword()).getDataRows(
        	sheet
         , 	rowIndexs)
}

def static "com.extosoft.keyword.ExcelKeyword.getMapOfKeyValueRows"(
    	Sheet sheet	
     , 	int keyRowIdx	
     , 	int valueRowIdx	) {
    (new com.extosoft.keyword.ExcelKeyword()).getMapOfKeyValueRows(
        	sheet
         , 	keyRowIdx
         , 	valueRowIdx)
}

def static "com.extosoft.keyword.ExcelKeyword.getCellValueByRangeAddress"(
    	Sheet sheet	
     , 	String topLeftAddress	
     , 	String rightBottomAddress	) {
    (new com.extosoft.keyword.ExcelKeyword()).getCellValueByRangeAddress(
        	sheet
         , 	topLeftAddress
         , 	rightBottomAddress)
}

def static "com.extosoft.keyword.ExcelKeyword.getCellValuesByRangeIndexes"(
    	Sheet sheet	
     , 	int rowInd1	
     , 	int colInd1	
     , 	int rowInd2	
     , 	int colInd2	) {
    (new com.extosoft.keyword.ExcelKeyword()).getCellValuesByRangeIndexes(
        	sheet
         , 	rowInd1
         , 	colInd1
         , 	rowInd2
         , 	colInd2)
}

def static "com.extosoft.keyword.ExcelKeyword.getColumnsByIndex"(
    	Sheet sheet	
     , 	java.util.List<java.lang.Integer> columnIndexes	) {
    (new com.extosoft.keyword.ExcelKeyword()).getColumnsByIndex(
        	sheet
         , 	columnIndexes)
}

def static "com.extosoft.keyword.ExcelKeyword.compareTwoExcels"(
    	Workbook workbook1	
     , 	Workbook workbook2	
     , 	boolean isValueOnly	) {
    (new com.extosoft.keyword.ExcelKeyword()).compareTwoExcels(
        	workbook1
         , 	workbook2
         , 	isValueOnly)
}

def static "com.extosoft.keyword.ExcelKeyword.compareTwoSheets"(
    	Sheet sheet1	
     , 	Sheet sheet2	
     , 	boolean isValueOnly	) {
    (new com.extosoft.keyword.ExcelKeyword()).compareTwoSheets(
        	sheet1
         , 	sheet2
         , 	isValueOnly)
}

def static "com.extosoft.keyword.ExcelKeyword.compareTwoRows"(
    	Row row1	
     , 	Row row2	
     , 	boolean isValueOnly	) {
    (new com.extosoft.keyword.ExcelKeyword()).compareTwoRows(
        	row1
         , 	row2
         , 	isValueOnly)
}

def static "com.extosoft.keyword.ExcelKeyword.compareTwoCells"(
    	Cell cell1	
     , 	Cell cell2	
     , 	boolean isValueOnly	) {
    (new com.extosoft.keyword.ExcelKeyword()).compareTwoCells(
        	cell1
         , 	cell2
         , 	isValueOnly)
}

def static "com.extosoft.keyword.ExcelKeyword.getExcelSheet"(
    	String filePath	) {
    (new com.extosoft.keyword.ExcelKeyword()).getExcelSheet(
        	filePath)
}

def static "com.extosoft.keyword.ExcelKeyword.getExcelSheet"(
    	Workbook wbs	) {
    (new com.extosoft.keyword.ExcelKeyword()).getExcelSheet(
        	wbs)
}

def static "com.extosoft.keyword.ExcelKeyword.compareTwoExcels"(
    	Workbook workbook1	
     , 	Workbook workbook2	) {
    (new com.extosoft.keyword.ExcelKeyword()).compareTwoExcels(
        	workbook1
         , 	workbook2)
}

def static "com.extosoft.keyword.ExcelKeyword.compareTwoSheets"(
    	Sheet sheet1	
     , 	Sheet sheet2	) {
    (new com.extosoft.keyword.ExcelKeyword()).compareTwoSheets(
        	sheet1
         , 	sheet2)
}

def static "com.extosoft.keyword.ExcelKeyword.compareTwoRows"(
    	Row row1	
     , 	Row row2	) {
    (new com.extosoft.keyword.ExcelKeyword()).compareTwoRows(
        	row1
         , 	row2)
}

def static "com.extosoft.keyword.ExcelKeyword.compareTwoCells"(
    	Cell cell1	
     , 	Cell cell2	) {
    (new com.extosoft.keyword.ExcelKeyword()).compareTwoCells(
        	cell1
         , 	cell2)
}

def static "com.extosoft.keyword.UtilityKeyword.FW_GenerateDateTime"() {
    (new com.extosoft.keyword.UtilityKeyword()).FW_GenerateDateTime()
}

def static "com.extosoft.keyword.UtilityKeyword.FW_TestStartTime"() {
    (new com.extosoft.keyword.UtilityKeyword()).FW_TestStartTime()
}

def static "com.extosoft.keyword.UtilityKeyword.FW_TestEndTime"() {
    (new com.extosoft.keyword.UtilityKeyword()).FW_TestEndTime()
}

def static "com.extosoft.keyword.UtilityKeyword.FW_DateExecuted"() {
    (new com.extosoft.keyword.UtilityKeyword()).FW_DateExecuted()
}

def static "com.extosoft.keyword.UtilityKeyword.FW_GetCurrentDateVersion"() {
    (new com.extosoft.keyword.UtilityKeyword()).FW_GetCurrentDateVersion()
}

def static "com.extosoft.keyword.UtilityKeyword.FW_GetDurationTime"() {
    (new com.extosoft.keyword.UtilityKeyword()).FW_GetDurationTime()
}

def static "com.extosoft.keyword.UtilityKeyword.FW_TestStartStep"(
    	String stepName	) {
    (new com.extosoft.keyword.UtilityKeyword()).FW_TestStartStep(
        	stepName)
}

def static "com.extosoft.keyword.UtilityKeyword.FW_TestStartStep"(
    	String stepName	
     , 	String stepValue	) {
    (new com.extosoft.keyword.UtilityKeyword()).FW_TestStartStep(
        	stepName
         , 	stepValue)
}

def static "com.extosoft.keyword.UtilityKeyword.FW_TestEndStep"() {
    (new com.extosoft.keyword.UtilityKeyword()).FW_TestEndStep()
}

def static "com.extosoft.keyword.UtilityKeyword.FW_CaptureScreenShot"() {
    (new com.extosoft.keyword.UtilityKeyword()).FW_CaptureScreenShot()
}
